const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');

random.setSeed(random.getRandomSeed());

const settings = {
  suffix: random.getSeed(),
  dimensions: [2048, 2048]
};

console.log(random.getSeed());

const sketch = () => {
  const margin = 400;
  const count = 6;

  const createGrid = () => {
    const points = [];


    for (let x = 0; x < count; x++) {
      for (let y = 0; y < count; y++) {
        const u = count <= 1 ? 0.5 : x / (count - 1);
        const v = count <= 1 ? 0.5 : y / (count - 1);
        points.push([u, v])
      }
    }

    return points;
  }

  const createTrapezoids = () => {
    const points = [];
    const gridPoints = [];
    const palette = random.pick(palettes);
    let pointSearchCount = 0;
    
    for (let x = 0; x < count; x++) {
      for (let y = 0; y < count; y++) { // declare y bigger to change grid height
        gridPoints.push([x / (count - 1), y / (count - 1)]);
      }
    }

    while (gridPoints.length > 1 && pointSearchCount < 1000) {
      const px1 = random.rangeFloor(0, count) / (count - 1);
      const py1 = random.rangeFloor(0, count) / (count - 1);

      const px2 = random.rangeFloor(0, count) / (count - 1);
      const py2 = random.rangeFloor(0, count) / (count - 1);

      const points1 = [];
      const points2 = [];

      if (px1 !== px2 && py1 !== py2) {
        let index1;
        let index2;

        gridPoints.forEach((gridPoint, index) => {
          if (gridPoint[0] === px1 && gridPoint[1] === py1) {
            points1.push(px1);
            points1.push(py1);
            index1 = index;
          }
        })

        gridPoints.forEach((gridPoint, index) => {
          if (gridPoint[0] === px2 && gridPoint[1] === py2) {
            points2.push(px2);
            points2.push(py2);
            index2 = index;
          }
        })

        if (points1.length !== 0 && points2.length !== 0) {
          gridPoints.splice(index1, 1);
          gridPoints.splice(index2, 1);
          points.push({
            points1,
            points2,
            average: (points1[1] + points2[1]) / 2,            
            color: random.pick(palette) + "FF" // + transparency
          });
        }
      }

      pointSearchCount++;
    }

    const compare = (a, b) => {
      const averageA = a.average;
      const averageB = b.average;

      let comparison = 0;
      if (averageA > averageB) {
        comparison = 1;
      } else {
        comparison = -1
      }

      return comparison;
    }

    points.sort(compare);

    return points;
  }

  const points = createGrid();
  const trapezoidPoints = createTrapezoids();

  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    // points.forEach(([u, v]) => {
    //   const x = lerp(margin, width - margin, u);
    //   const y = lerp(margin, height - margin, v);

    //   context.beginPath();
    //   context.arc(x, y, 30, 0, Math.PI * 2, false);
    //   context.strokeStyle = 'black';
    //   context.lineWidth = 2;
    //   context.stroke();
    // });

    trapezoidPoints.forEach(data => {
      const {
        points1,
        points2,
        average,
        color
      } = data;

      const [a, b] = points1;
      const [c, d] = points2;

      const x1 = lerp(margin, width - margin, a)
      const y1 = lerp(margin, height - margin, b)

      const x2 = lerp(margin, width - margin, c)
      const y2 = lerp(margin, height - margin, d)

      const bottom = lerp(margin, width - margin, 1)

      context.beginPath();
      context.moveTo(x1, y1);
      context.lineTo(x2, y2);
      context.lineTo(x2, bottom);
      context.lineTo(x1, bottom);
      context.lineTo(x1, y1);
      context.closePath();

      context.lineWidth = 40;
      context.fillStyle = color;
      context.strokeStyle = 'white';
      context.stroke();
      context.fill();
    });
  };
};

canvasSketch(sketch, settings);
